import 'dart:io';

import 'package:path_provider/path_provider.dart';

class Points {
  List<String> scoreS = List.filled(5, 'Empty');
  int firstStep;
  int secondStep;
  int finalStep;

  Points(this.firstStep, this.secondStep, this.finalStep);

  int sumAllParts() => firstStep + secondStep + finalStep;

  void addToScoreBoard() {
    if (scoreS.length == 5) {
      scoreS = List<String>.from(removeAndReturnList(scoreS, sumAllParts()));
    } else {
      scoreS.insert(0, '${sumAllParts()}');
    }
  }

  List<String> removeAndReturnList(List<String> inputList, int addItem) {
    var tempOutList = new List<String>.from(inputList);
    tempOutList.insert(0, '$addItem ');
    if (tempOutList.length > 5) {
      while (tempOutList.length > 5) {
        tempOutList.removeAt(tempOutList.length - 1);
      }
      return tempOutList.toList();
    } else {
      return tempOutList.toList();
    }
  }

  Future<void> splitToList(Future<String> inputString) async {
    var tempListS = new List<String>.from(scoreS);
    var tempListW = new List<String>.from(scoreS);
    String noFutureString = '';
    //inputString.then((value) {noFutureString = value.toString();});
    noFutureString = await inputString;
    //scoreS = noFutureString.split(" ");
    tempListW = noFutureString.split(" ");
    tempListS.insert(0, tempListW[0]);
    tempListS.removeWhere(
        (element) => element.isEmpty || element == ' '); //remove trash
    if (tempListS.length < 5) {
      for (int i = tempListS.length; i < 5; i++) {
        tempListS.add('Empty');
      }
    } else if (tempListS.length > 5) {
      while (tempListS.length > 5) {
        tempListS.removeLast();
      }
    }
    scoreS = List.from(tempListS);
  }

  static Future<String> get _localPath async {
    //directly from Flutter dev page
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  static Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/wyniki.txt');
  }

  static Future<void> writeString(String score) async {
    final file = await _localFile;
    // Write the file
    return file.writeAsStringSync('$score', mode: FileMode.write);
  }

  static Future<void> deleteFile() async {
    final file = await _localFile;
    file.delete();
  }

  static Future<String> readScoreString() async {
    final file = await _localFile;
    final stringline = await file.readAsString();
    print(stringline);
    return stringline;
  }

  void writeToFile() {
    String tempS = '';
    for (int i = 0; i < scoreS.length; i++) {
      tempS += '${scoreS[i]}';
      if (i != scoreS.length - 1) {
        tempS += ' ';
      }
    }
    deleteFile();
    writeString(tempS);
  }
}
