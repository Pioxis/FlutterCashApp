import 'dart:async'; //to use in future function
import 'dart:convert'; //to parse json string
import 'dart:math'; //to use Random() function

import 'package:animated_icon_button/animated_icon_button.dart';
import 'package:flutter/material.dart'; //to build layout
import 'package:flutter/services.dart' show rootBundle; //to import json file
import 'package:fluttericon/rpg_awesome_icons.dart'; //more icons to category buttons
import 'package:pioxis_app/points.dart';

//to prepare wallet construction to pass coins from first to third screen
import 'firstStep.dart';

List<ProductList>? listProducts; //all products from json file of products
List<ProductList> shoppingList = new List<ProductList>.empty();
List<ProductList> shoppingCartList = new List<ProductList>.empty();
bool shopLGen = false;
//switch to allow make change of wallet amount only once,
// prevent change when screen is reloading
bool exitenter = false;
var walletVal;
//construct object with default values
var walletCashVar = CashValue(0, 0, 0, 0, 0, 0, 0, 0);
Points _gamePointsSecond = Points(0, 0, 0);

//class of products imported from json file
class ProductList {
  //product list structure
  int id;
  int value;
  int volume;
  String category;
  String name;
  String unit;
  int priority = 0;

  ProductList(
      {required this.id,
      required this.value,
      required this.volume,
      required this.category,
      required this.name,
      required this.unit});

  factory ProductList.from(ProductList objectCopy) {
    return ProductList(
      id: objectCopy.id,
      value: objectCopy.value,
      volume: objectCopy.volume,
      category: objectCopy.category,
      name: objectCopy.name,
      unit: objectCopy.unit,
    );
  }

  factory ProductList.fromJson(dynamic json) {
    return ProductList(
      id: json['id'] as int,
      value: json['value'] as int,
      volume: json['volume'] as int,
      category: json['category'] as String,
      name: json['name'] as String,
      unit: json['unit'] as String,
    );
  }

  static int sumVal(List<ProductList> s) {
    ///only for products from shopping cart to sum value of all products
    int outSum = 0;
    for (int i = 0; i < s.length; i++) {
      outSum += s[i].value;
    }
    return outSum;
  }

  @override
  String toString() {
    return '{ ${this.id}, ${this.value}, ${this.volume}, '
        '${this.category}, ${this.name}, ${this.unit},}';
  }
}

//import from json file products to selected list
Future<List<ProductList>?> getData() async {
  String jnCode = await rootBundle.loadString('lib/jnDir/products.json');
  var data = json.decode(jnCode);
  var rest = data["products"] as List;
  listProducts =
      rest.map<ProductList>((json) => ProductList.fromJson(json)).toList();
  print("List Size: ${listProducts?.length}"); //Debug Value of list amount
  return listProducts;
}

class AnimationsInDialog extends StatefulWidget {
  final String catVarName; //to transport category name
  AnimationsInDialog({required this.catVarName});

  @override
  _AnimatedDialog createState() => new _AnimatedDialog();
}

class _AnimatedDialog extends State<AnimationsInDialog>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  bool switchAnim = true;
  String catNameVal = '';

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 400),
    );
  }

  @override
  Widget build(BuildContext context) {
    catNameVal = widget.catVarName;
    return AlertDialog(
      title: Center(
        child: Text(
          catNameVal,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.black,
            fontSize: 29,
          ),
        ),
      ),
      content: setupGridDialView(listProducts!, catNameVal, switchAnim),
      actions: <Widget>[
        Center(
          child: FloatingActionButton(
            tooltip:
                'Shopping Cart\nYou can remove products here\nSlide to remove',
            enableFeedback: true,
            backgroundColor: Colors.orange,
            elevation: 15,
            onPressed: null,
            child: AnimatedIconButton(
              animationController: _controller,
              splashRadius: 28,
              hoverColor: Colors.transparent,
              onPressed: () {
                showListPanel(context, 'Shopping Cart',
                    setupDialShoppingCartList, shoppingCartList, false, false);
              },
              size: 30,
              duration: Duration(milliseconds: 400),
              icons: [
                AnimatedIconItem(
                  icon: Icon(
                    Icons.add_shopping_cart_rounded,
                    color: Colors.black,
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget setupGridDialView(List<ProductList> allProducts, String categoryName,
      bool switchAnimSetter) {
    //grid view for category products
    List<ProductList> catList = new List<ProductList>.empty();
    catList =
        categoryGridContentViewListFill(allProducts, categoryName, catList);
    return Container(
      height: 350.0, // Change as per your requirement
      width: 500.0, // Change as per your requirement
      child: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 22,
          mainAxisSpacing: 22,
        ),
        itemBuilder: (context, position) {
          return Tooltip(
            message: 'Tap to add ${catList[position].name} to shopping cart',
            enableFeedback: true,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                elevation: 10,
                backgroundColor: Colors.grey.shade900,
                foregroundColor: Colors.deepOrange,
              ),
              onPressed: () {
                setState(
                  () {
                    switchAnim ? _controller.forward() : _controller.reverse();
                    switchAnim = !switchAnim;
                  },
                );
                addProdToCart(catList, position);
              },
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    categoryIcon(categoryName, 32),
                    Text(
                      '${catList[position].name}',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    Text(
                      '${catList[position].volume} ${catList[position].unit}',
                      style: TextStyle(color: Colors.white, fontSize: 12),
                      textAlign: TextAlign.center,
                    ),
                    Text(
                      '${catList[position].value} &',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 13,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center,
                    )
                  ],
                ),
              ),
            ),
          );
        },
        itemCount: catList.length,
      ),
    );
  }

  Widget setupDialShoppingCartList(List<ProductList> productListInCart) {
    //create alertdialog for Shopping Cart List
    return Container(
      height: 300.0, // Change as per your requirement
      width: 400.0, // Change as per your requirement
      child: ListView.builder(
        itemCount: productListInCart.length,
        shrinkWrap: true,
        itemBuilder: (context, position) {
          //final product = productListInCart[position].name.toString();
          return Dismissible(
            key: ObjectKey(productListInCart),
            onDismissed: (direction) {
              setState(() {
                productListInCart.removeAt(position);
                //in future to change to properly change state in the same view
                Navigator.pop(context);
              });
            },
            background: Card(
              color: Colors.red,
              elevation: 3,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0),
              ),
            ),
            child: Card(
              elevation: 3,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: ListTile(
                visualDensity: VisualDensity.compact,
                horizontalTitleGap: 5,
                isThreeLine: false,
                title: Text(
                  '${productListInCart[position].name}',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                subtitle: Row(
                  children: <Widget>[
                    Text(
                      '${productListInCart[position].volume} ${productListInCart[position].unit}',
                    )
                  ],
                ),
                trailing: Text(
                  '${productListInCart[position].value} &',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 21),
                ),
                leading: categoryIcon(productListInCart[position].category, 35),
                onTap: null,
              ),
            ),
          );
        },
      ),
    );
  }
}

//main screen controller
class SecondStep extends StatelessWidget {
  const SecondStep({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (exitenter == false) {
      final argsRec = ModalRoute.of(context)?.settings.arguments as Map;
      walletCashVar = argsRec['cashWalletAmount'];
      _gamePointsSecond = argsRec['gamePoints'];
      debugPrint(
        'W: ${walletValue(walletCashVar)} '
        'G: ${_gamePointsSecond.sumAllParts()}',
      );
    }
    walletVal = walletValue(walletCashVar);
    print('Wallet val: $walletVal');
    return new WillPopScope(
      onWillPop: () async => false,
      child: new Scaffold(
        resizeToAvoidBottomInset: false,
        //prevent of horizontal view
        appBar: AppBar(
          elevation: 5,
          title: Text('Shop'),
          titleSpacing: 2,
          shadowColor: Colors.grey,
          actions: <Widget>[
            Tooltip(
              message: 'Value of cash in your wallet',
              enableFeedback: true,
              child: ElevatedButton.icon(
                //ammount of wallet
                style: ElevatedButton.styleFrom(elevation: 15),
                onPressed: () {},
                label: Text(
                  '$walletVal &',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 24,
                  ),
                ),
                icon: Icon(Icons.account_balance_wallet),
              ),
            )
          ],
        ),
        body: FutureBuilder(
          future: getData(),
          builder: (context, snapshot) {
            return snapshot.data != null
                //if list of all products can't be load then we have circle loader
                ? mainScreenBody(listProducts!, context)
                : Center(child: CircularProgressIndicator());
          },
        ),
        floatingActionButtonLocation: shopListIsGenerated()
            ? FloatingActionButtonLocation.centerFloat
            : FloatingActionButtonLocation.miniCenterDocked,
        floatingActionButton: shopListIsGenerated()
            ? Visibility(
                child: FloatingActionButton.large(
                  tooltip: 'Generate new shopping list',
                  enableFeedback: true,
                  onPressed: shopLGen
                      ? null
                      : () {
                          shoppingCartList = new List.from(shoppingList);
                          shoppingCartList.clear();
                          shopLGen = true;
                          exitenter = true;
                          generateShoppingList();
                          Navigator.pushNamedAndRemoveUntil(
                            context,
                            'secback',
                            (_) => false,
                          );
                          showListPanel(
                            context,
                            'Shopping List',
                            setupDialShoppingList,
                            shoppingList,
                            true,
                            false,
                          );
                        },
                  child: Icon(
                    Icons.post_add_outlined,
                    size: 64,
                  ),
                  backgroundColor: Colors.green,
                  elevation: 30,
                  heroTag: null,
                  splashColor: Colors.lightGreenAccent,
                ),
                visible: shopLGen ? false : true,
              )
            : Padding(
                //after generate shopping list
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    FloatingActionButton.large(
                      //shopping list floating button
                      tooltip: 'Shopping List',
                      enableFeedback: true,
                      onPressed:
                          //disabled if shopping list is not created
                          shopListIsGenerated()
                              ? null
                              : () {
                                  showListPanel(
                                    context,
                                    'Shopping List',
                                    setupDialShoppingList,
                                    shoppingList,
                                    true,
                                    false,
                                  );
                                },
                      child: Icon(
                        Icons.list_alt_rounded,
                        size: 64,
                      ),
                      backgroundColor:
                          shopListIsGenerated() ? Colors.grey : Colors.purple,
                      splashColor: Colors.purpleAccent,
                      elevation: 30,
                      heroTag: null,
                    ),
                    FloatingActionButton.large(
                      enableFeedback: true,
                      tooltip: 'Accept products and go to cash register',
                      onPressed: shopListIsGenerated()
                          ? null
                          : () {
                              showListPanel(
                                context,
                                'Accept Final Cart',
                                setupDialShoppingCartListOnlyView,
                                shoppingCartList,
                                false,
                                true,
                              );
                            },
                      child: Icon(
                        Icons.shopping_basket_rounded,
                        size: 64,
                      ),
                      backgroundColor:
                          shopListIsGenerated() ? Colors.grey : Colors.green,
                      splashColor: Colors.lightGreen,
                      elevation: 30,
                      heroTag: null,
                    ),
                  ],
                ),
              ),
        drawer: Drawer(
          child: ListView(
            //padding: EdgeInsets.zero,
            children: [
              const SizedBox(
                height: 60.0,
                child: DrawerHeader(
                  child: Text(
                    'Menu',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 19),
                  ),
                ),
              ),
              Card(
                elevation: 5,
                child: ListTile(
                  leading: Icon(Icons.save),
                  title: shopListIsGenerated()
                      ? Text(
                          'Shopping List',
                          style: TextStyle(
                            color: Colors.grey,
                            decoration: TextDecoration.lineThrough,
                            decorationStyle: TextDecorationStyle.solid,
                            decorationThickness: 4.0,
                          ),
                        )
                      : Text('Shopping List'),
                  onTap: shopListIsGenerated()
                      ? null
                      : () {
                          showListPanel(
                            context,
                            'Shopping List',
                            setupDialShoppingList,
                            shoppingList,
                            true,
                            false,
                          );
                        },
                ),
              ),
              Card(
                elevation: 5,
                child: ListTile(
                  leading: Icon(Icons.settings_rounded),
                  title: Text('Settings'),
                  onTap: () {
                    Navigator.pushNamed(context, '/settings');
                  },
                ),
              ),
              Card(
                elevation: 5,
                child: ListTile(
                  leading: Icon(Icons.cancel),
                  title: Text('Exit to Menu'),
                  onTap: () {
                    exitenter = false;
                    Navigator.pushNamedAndRemoveUntil(
                        context, '/', (_) => false);
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

@override
Widget mainScreenBody(List<ProductList> listOfProd, BuildContext contMain) {
  //Build main screen body widget
  return Container(
    child: Center(
      child: Column(
        children: <Widget>[
          Expanded(
            flex: 5,
            //------------------------------------------- 1 ROW
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Spacer(flex: 1),
                categoryButton(
                  contMain,
                  'Bakery',
                  Icon(
                    Icons.breakfast_dining_rounded,
                    size: 42,
                  ),
                  'Bakery',
                ),
                Spacer(flex: 1),
                categoryButton(
                  contMain,
                  'Drinks',
                  Icon(
                    Icons.local_drink_rounded,
                    size: 42,
                  ),
                  'Drinks',
                ),
                Spacer(flex: 1),
                categoryButton(
                  contMain,
                  'Baking',
                  Icon(
                    Icons.bakery_dining_rounded,
                    size: 42,
                  ),
                  'Baking',
                ),
                Spacer(flex: 1),
              ],
            ),
          ),
          Expanded(
            flex: 5,
            //------------------------------------------- 2 ROW
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Spacer(flex: 1),
                categoryButton(
                  contMain,
                  'Spices',
                  Icon(
                    Icons.gradient_rounded,
                    size: 42,
                  ),
                  'Spices',
                ),
                Spacer(flex: 1),
                categoryButton(
                  contMain,
                  'Pasta',
                  Icon(
                    Icons.dinner_dining_rounded,
                    size: 42,
                  ),
                  'Pasta',
                ),
                Spacer(flex: 1),
                categoryButton(
                  contMain,
                  'Grain',
                  Icon(
                    Icons.scatter_plot_rounded,
                    size: 42,
                  ),
                  'Grains',
                ),
                Spacer(flex: 1),
              ],
            ),
          ),
          Expanded(
            flex: 5,
            //------------------------------------------- 3 ROW
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Spacer(flex: 1),
                categoryButton(
                  contMain,
                  'Dairy',
                  Icon(
                    RpgAwesome.cheese,
                    size: 42,
                  ),
                  'Dairy',
                ),
                Spacer(flex: 1),
                categoryButton(
                  contMain,
                  'Vegetable',
                  Icon(
                    RpgAwesome.carrot,
                    size: 42,
                  ),
                  'Vegetable',
                ),
                Spacer(flex: 1),
                categoryButton(
                  contMain,
                  'Fruit',
                  Icon(
                    RpgAwesome.apple,
                    size: 42,
                  ),
                  'Fruits',
                ),
                Spacer(flex: 1),
              ],
            ),
          ),
          Expanded(
            flex: 5,
            //------------------------------------------- 4 ROW
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Spacer(flex: 1),
                categoryButton(
                    contMain, 'Meat', Icon(RpgAwesome.meat, size: 42), 'Meat'),
                Spacer(flex: 1),
                categoryButton(contMain, 'Sanitary Articles',
                    Icon(Icons.local_pharmacy_rounded, size: 42), 'Sanitary'),
                Spacer(flex: 1),
                categoryButton(contMain, 'Misc',
                    Icon(Icons.local_activity_rounded, size: 42), 'Misc.'),
                Spacer(flex: 1),
              ],
            ),
          ),
          Spacer(flex: 1),
          Expanded(
            flex: 2,
            child: Card(
              child: ListTile(),
            ),
          )
        ],
      ),
    ),
  );
}

Widget categoryButton(BuildContext contMainBut, String categoryName,
    Icon categoryIcon, String viewText) {
  var sizeBut = 100.0;
  return Tooltip(
    message: 'Shelf of $viewText Category',
    enableFeedback: true,
    child: ElevatedButton(
      style: ElevatedButton.styleFrom(
        minimumSize: Size(sizeBut, sizeBut),
        maximumSize: Size(sizeBut, sizeBut),
        elevation: 8,
        shadowColor: Colors.grey,
      ),
      onPressed:
          shopLGen ? () => showCatProdList(contMainBut, categoryName) : null,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[categoryIcon, Text(viewText)],
      ),
    ),
  );
}

List<ProductList> rndFillList(
    List<ProductList> allProds, List<ProductList> fillList) {
  //function to fill list of shopping list
  Random random = new Random();
  //random cound of shopping list elements 5-10 items
  var shopListItems = random.nextInt(5) + 5;
  //random count of item to buy
  var amountVar = random.nextInt(6) + 1;
  //priority to buy -> higher priority, more points in scoreboard, range 1-3
  var priorVal = random.nextInt(2) + 1;
  //amount of products from json product list
  var iterFullList = allProds.length - 1;
  //random number from 0 to max product list amount of items
  var randInt = random.nextInt(iterFullList);
  //fill list of first product, index: 0
  fillList.add(allProds[randInt]);
  fillList[0].volume = fillList[0].volume * amountVar;
  fillList[0].value = fillList[0].value * amountVar;
  fillList[0].priority = priorVal;
  for (int i = 0; i < shopListItems; i++) {
    //add random number items || arrange from 5 - 25
    //change number to add random items to shopping list
    randInt = random.nextInt(iterFullList);
    amountVar = random.nextInt(4) + 1;
    priorVal = random.nextInt(3) + 1;
    //if we have the same ID number of product then draw another numebr
    if (fillList.any((element) => element.id == randInt)) {
      //back to previous state to prevent end iterate and generate empty list
      i -= 1;
    } else {
      //If list don't have rand item insde then add as new item inside list
      fillList.add(allProds[randInt]);
      fillList[i + 1].volume = fillList[i + 1].volume * amountVar;
      fillList[i + 1].value = fillList[i + 1].value * amountVar;
      fillList[i + 1].priority = priorVal;
    }
  }
  return fillList;
} //alg 2

void showListPanel(
    BuildContext contextSh,
    String title,
    Widget buildListTiles(List<ProductList> s),
    List<ProductList> filList,
    bool visiblityPrior,
    bool actionVisible) {
  //build dialog for any List<ProductList>
  showDialog(
    barrierDismissible: !actionVisible,
    barrierColor: actionVisible ? Colors.black87 : Colors.black54,
    context: contextSh,
    builder: (BuildContext context) {
      return AlertDialog(
        actions: <Widget>[
          Visibility(
            child: Tooltip(
              message: 'Add more or remove some\nYou can change your mind',
              enableFeedback: true,
              child: ElevatedButton.icon(
                style: ElevatedButton.styleFrom(backgroundColor: Colors.red),
                label: Text(
                  'Back',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                icon: Icon(Icons.arrow_back_rounded),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ),
            visible: actionVisible,
          ),
          Visibility(
            child: Directionality(
              textDirection: TextDirection.rtl,
              child: Tooltip(
                message: 'Accept and go next step',
                enableFeedback: true,
                child: ElevatedButton.icon(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.green,
                  ),
                  label: Text(
                    'Save',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  icon: Icon(Icons.done_rounded),
                  onPressed: () => {
                    generatePointsSecStep(),
                    Navigator.pop(context), //close AlertDailog
                    Navigator.pushNamedAndRemoveUntil(
                      contextSh, '/final', (_) => false,
                      arguments: {
                        'walletCashVar': walletCashVar,
                        'shoppingCartList': shoppingCartList,
                        '_gamePointsSecond': _gamePointsSecond
                      }, //cash, list and points
                    )
                  },
                ),
              ),
            ),
            visible: actionVisible,
          ),
        ],
        title: Center(
          child: Column(
            children: <Widget>[
              Text(
                title,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                  fontSize: 29,
                ),
              ),
              Visibility(
                child: Text(
                  'Priority:',
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                visible: visiblityPrior,
              ),
              Visibility(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'High ',
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                      ),
                    ),
                    priorityIcon(1, 16),
                    Text(
                      ' Medium ',
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                      ),
                    ),
                    priorityIcon(2, 16),
                    Text(
                      ' Low ',
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                      ),
                    ),
                    priorityIcon(3, 16)
                  ],
                ),
                visible: visiblityPrior,
              )
            ],
          ),
        ),
        content: buildListTiles(filList),
      );
    },
  );
}

void showCatProdList(BuildContext contextCat, String categoryName) {
  //create alert dialog for category products
  showDialog(
    useSafeArea: true,
    context: contextCat,
    builder: (BuildContext context) {
      return AnimationsInDialog(
        catVarName: categoryName,
      );
    },
  );
}

bool shopListIsGenerated() {
  if (shoppingList.isEmpty) {
    return true;
  } else {
    return false;
  }
}

void generateShoppingList() {
  //Create new instance of List<ProductList>
  shoppingList = List.from(listProducts!);
  //clear from items of AllProducts
  shoppingList.clear();
  //create new items in new list
  shoppingList = rndFillList(listProducts!, shoppingList);
  debugPrint('Shopping list items: ' + (shoppingList.length).toString());
  shoppingList.sort(
    (a, b) {
      //sort items by category a-z
      return a.category
          .toString()
          .toLowerCase()
          .compareTo(b.category.toString().toLowerCase());
    },
  );
}

Widget setupDialShoppingList(List<ProductList> shoppingListDial) {
  //create alertdialog for shopping list
  return Container(
    height: 500.0, // Change as per your requirement
    width: 400.0, // Change as per your requirement
    child: ListView.builder(
      itemCount: shoppingListDial.length,
      shrinkWrap: true,
      itemBuilder: (context, position) {
        return Card(
          elevation: 3,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
          child: ListTile(
            visualDensity: VisualDensity.compact,
            //horizontalTitleGap: 5,
            isThreeLine: false,
            title: Text(
              '${shoppingListDial[position].name}',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            subtitle: Row(
              children: <Widget>[
                priorityIcon(shoppingListDial[position].priority, 16)
              ],
            ),
            trailing: Text(
              '${shoppingListDial[position].volume} '
              '${shoppingListDial[position].unit}',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 19),
            ),
            leading: categoryIcon(shoppingListDial[position].category, 35),
            onTap: null,
          ),
        );
      },
    ),
  );
}

List<ProductList> categoryGridContentViewListFill(
    final List<ProductList> allProducts,
    String categoryName,
    List<ProductList> catList) {
  catList = List.from(allProducts);
  catList.clear();
  for (int i = 0; i < allProducts.length; i++) {
    if (allProducts[i].category == categoryName) {
      catList.add(allProducts[i]);
    } else if (allProducts[i].category != 'Bakery' &&
        allProducts[i].category != 'Drinks' &&
        allProducts[i].category != 'Baking' &&
        allProducts[i].category != 'Spices' &&
        allProducts[i].category != 'Pasta' &&
        allProducts[i].category != 'Grain' &&
        allProducts[i].category != 'Dairy' &&
        allProducts[i].category != 'Vegetable' &&
        allProducts[i].category != 'Fruit' &&
        allProducts[i].category != 'Meat' &&
        allProducts[i].category != 'Sanitary Articles' &&
        categoryName == 'Misc') {
      catList.add(allProducts[i]);
    }
  }
  return catList;
}

Icon categoryIcon(String catName, double sizeIc) {
  Icon retIc = Icon(null);
  if (catName == 'Bakery') {
    retIc = Icon(
      Icons.breakfast_dining_rounded,
      size: sizeIc,
      color: Colors.deepOrange,
    );
  } else if (catName == 'Drinks') {
    retIc = Icon(
      Icons.local_drink_rounded,
      size: sizeIc,
      color: Colors.blue,
    );
  } else if (catName == 'Baking') {
    retIc = Icon(
      Icons.bakery_dining_rounded,
      size: sizeIc,
      color: Colors.brown,
    );
  } else if (catName == 'Spices') {
    retIc = Icon(
      Icons.gradient_rounded,
      size: sizeIc,
      color: Colors.pinkAccent,
    );
  } else if (catName == 'Pasta') {
    retIc = Icon(
      Icons.dinner_dining_rounded,
      size: sizeIc,
      color: Colors.orangeAccent,
    );
  } else if (catName == 'Grain') {
    retIc = Icon(
      Icons.scatter_plot_rounded,
      size: sizeIc,
      color: Colors.amber,
    );
  } else if (catName == 'Dairy') {
    retIc = Icon(
      RpgAwesome.cheese,
      size: sizeIc,
      color: Colors.cyan,
    );
  } else if (catName == 'Vegetable') {
    retIc = Icon(
      RpgAwesome.carrot,
      size: sizeIc,
      color: Colors.lightGreen,
    );
  } else if (catName == 'Fruit') {
    retIc = Icon(
      RpgAwesome.apple,
      size: sizeIc,
      color: Colors.deepOrangeAccent,
    );
  } else if (catName == 'Meat') {
    retIc = Icon(
      RpgAwesome.meat,
      size: sizeIc,
      color: Colors.redAccent,
    );
  } else if (catName == 'Sanitary Articles') {
    retIc = Icon(
      Icons.local_pharmacy_rounded,
      size: sizeIc,
      color: Colors.indigoAccent,
    );
  } else {
    retIc = Icon(
      Icons.local_activity_rounded,
      size: sizeIc,
      color: Colors.pink,
    );
  }
  return retIc;
}

Icon priorityIcon(int priorityVal, double sizeIc) {
  if (priorityVal == 1) {
    return Icon(
      Icons.circle,
      size: sizeIc,
      color: Colors.red,
    );
  } //high
  else if (priorityVal == 2) {
    return Icon(
      Icons.circle,
      size: sizeIc,
      color: Colors.orange,
    );
  } //medium
  else if (priorityVal == 3) {
    return Icon(
      Icons.circle,
      size: sizeIc,
      color: Colors.lightGreen,
    );
  } //low
  else {
    return Icon(
      Icons.circle,
      size: sizeIc,
      color: Colors.blueGrey,
    );
  } //no priority
}

int findIndexInList(final List<ProductList> inputList, final String nameItem) {
  //while return -1 then not found item
  var index = inputList.indexWhere((element) => element.name == nameItem);
  return index;
}

Widget setupDialShoppingCartListOnlyView(List<ProductList> productListInCart) {
  //create alertdialog for Shopping Cart List
  return Container(
    height: 300.0, // Change as per your requirement
    width: 400.0, // Change as per your requirement
    child: ListView.builder(
      itemCount: productListInCart.length,
      shrinkWrap: true,
      itemBuilder: (context, position) {
        return Card(
          elevation: 3,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
          child: ListTile(
            visualDensity: VisualDensity.compact,
            horizontalTitleGap: 5,
            isThreeLine: false,
            title: Text(
              '${productListInCart[position].name}',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            subtitle: Row(
              children: <Widget>[
                Text(
                  '${productListInCart[position].volume} '
                  '${productListInCart[position].unit}',
                )
              ],
            ),
            trailing: Text(
              '${productListInCart[position].value} &',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 21),
            ),
            leading: categoryIcon(productListInCart[position].category, 35),
            onTap: null,
          ),
        );
      },
    ),
  );
}

void addProdToCart(List<ProductList> categoryList, int pos) {
  int indx = findIndexInList(shoppingCartList, categoryList[pos].name);
  ProductList temp = new ProductList.from(categoryList[pos]);
  if (indx >= 0) {
    //if product exist, then add only parameters
    shoppingCartList[indx].volume += temp.volume;
    shoppingCartList[indx].value += temp.value;
  } else {
    //if product dont exist in list then add product with default parameters
    shoppingCartList.add(temp);
  }
}

void generatePointsSecStep() {
  double l = 0.0, m = 0.0, w = 0.0;
  int tooMuch = 0;
  for (int z = 0; z < shoppingList.length; z++) {
    if (shoppingCartList.any((e) => e.name == shoppingList[z].name)) {
      //if any element in cart equal to shopping list
      l += (shoppingList[z].priority * shoppingList[z].volume).toDouble();
      m += (shoppingList[z].priority).toDouble();
      //remove points for too much elements in cart
      if ((shoppingCartList.firstWhere(
              (element) => element.name == shoppingList[z].name)).volume >
          shoppingList[z].volume) {
        tooMuch -= (15 *
                ((shoppingCartList.firstWhere(
                        (element) => element.name == shoppingList[z].name))
                    .volume) -
            shoppingList[z].volume);
      }
    } else {
      //negative points
      l += ((shoppingList[z].priority * shoppingList[z].volume) * (-1.0))
          .toDouble();
      m += shoppingList[z].priority.toDouble();
    }
  }
  w += (l / m) * 250;
  debugPrint('l: $l m: $m tooM: $tooMuch w: $w');
  //round to up
  _gamePointsSecond.secondStep += w.round();
  //add negative points for too much elements
  _gamePointsSecond.secondStep += tooMuch;
  //dont make negativ result, if negativ then set to 0
  if (_gamePointsSecond.secondStep < 0) {
    _gamePointsSecond.secondStep = 0;
  }
  debugPrint('Points: ${_gamePointsSecond.secondStep}');
} //alg 3
