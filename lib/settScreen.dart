import 'package:flutter/material.dart';
import 'package:restart_app/restart_app.dart';

class SettingsScreen extends StatelessWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Settings'),
          centerTitle: true,
        ),
        body: settScreen(context));
  }
}

Widget settScreen(BuildContext contSett) {
  return Center(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Tooltip(
          message: 'Restart App to default values',
          enableFeedback: true,
          child: ElevatedButton.icon(
            onPressed: () {
              print('Pressed: ' + 'RESTART');
              Restart.restartApp(webOrigin: '/');
            },
            icon: Icon(Icons.restart_alt_rounded),
            label: Text('RESTART APP'),
          ),
        ),
        Text('\n'),
        Tooltip(
          message: 'Tap to see Score Board',
          enableFeedback: true,
          child: ElevatedButton.icon(
            onPressed: () {
              print('Pressed: ' + 'Score Board');
              Navigator.pushReplacementNamed(contSett, '/score',
                  arguments: true);
            },
            icon: Icon(Icons.format_list_numbered_rounded),
            label: Text('SCORE BOARD'),
          ),
        ),
      ],
    ),
  );
}
