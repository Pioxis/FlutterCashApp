import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class FirstScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown],
    );
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        elevation: 15,
        title: const Center(
          child: Text('MENU'),
        ),
      ),
      body: startScreen(context),
    );
  }
}

Widget startScreen(BuildContext cont) {
  return Column(
    children: <Widget>[
      Spacer(flex: 1),
      Expanded(
        flex: 5,
        child: Align(
          alignment: AlignmentDirectional.center,
          child: Image.asset(
            //change icon depends of system theme
            iconString(cont),
          ),
        ),
      ),
      Expanded(
        flex: 4,
        child: Align(
          alignment: AlignmentDirectional.center,
          child: mainMenuButtons(cont),
        ),
      ),
      Spacer(flex: 2),
      Expanded(
        flex: 2,
        child: Align(
          alignment: Alignment.bottomCenter,
          child: lowerButtonSection(cont),
        ),
      ),
      Spacer(flex: 1),
    ],
  );
}

Widget mainMenuButtons(BuildContext mainCont) {
  return Column(
    children: <Widget>[
      Expanded(
        child: _buildButtonStart(
          Colors.amber,
          Icons.arrow_forward_ios,
          'START',
          Size(200, 70),
          mainCont,
          'start',
        ),
      ),
      Expanded(
        child: Tooltip(
          message: 'Turn off app',
          enableFeedback: true,
          child: _exitButtonAction(
            Colors.amber,
            Icons.cancel,
            'EXIT',
            Size(200, 50),
            mainCont,
          ),
        ),
      ),
    ],
  );
}

Widget lowerButtonSection(BuildContext contLower) {
  return Row(
    children: <Widget>[
      Expanded(
        child: _buildButtonSettings(
          Colors.amber,
          Icons.polymer,
          'SETTINGS',
          Size(75, 50),
          contLower,
          '/settings',
        ),
      ),
      Expanded(
        child: _dialogShowButton(
          Colors.amber,
          Icons.alternate_email_sharp,
          'CONTACT',
          Size(75, 50),
          contLower,
        ),
      ),
    ],
  );
}

Column _buildButtonStart(Color color, IconData icon, String label,
    Size minimumSizeValue, BuildContext butCont,
    [String? routers]) {
  return Column(
    mainAxisSize: MainAxisSize.min,
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Container(
        margin: const EdgeInsets.only(top: 12),
        child: ElevatedButton(
          onPressed: () {
            print('Pressed: ' + label);

            // (_) => false cause previous context is removed from
            // memory and app cant back to it
            Navigator.pushNamedAndRemoveUntil(butCont, routers!, (_) => false);
          },
          //onPressed: null, //button is disabled when :null
          child: Column(
            children: <Widget>[
              Icon(icon),
              Text(label),
            ],
          ),
          style: ElevatedButton.styleFrom(
            minimumSize: minimumSizeValue,
            elevation: 30,
          ),
        ),
      ),
    ],
  );
}

Column _buildButtonSettings(Color color, IconData icon, String label,
    Size minimumSizeValue, BuildContext butCont,
    [String? routers]) {
  return Column(
    mainAxisSize: MainAxisSize.min,
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Container(
        margin: const EdgeInsets.only(top: 12),
        child: ElevatedButton(
          onPressed: () {
            print('Pressed: ' + label);
            Navigator.pushNamed(butCont, routers!);
          },
          child: Column(
            children: <Widget>[
              Icon(icon),
              Text(label),
            ],
          ),
          style: ElevatedButton.styleFrom(
            minimumSize: minimumSizeValue,
            elevation: 30,
          ),
        ),
      ),
    ],
  );
}

Column _dialogShowButton(Color color, IconData icon, String label,
    Size minimumSizeValue, BuildContext butCont) {
  return Column(
    mainAxisSize: MainAxisSize.min,
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Container(
        margin: const EdgeInsets.only(top: 12),
        child: ElevatedButton(
          onPressed: () => showDialog<String>(
            context: butCont,
            builder: (BuildContext context) => AlertDialog(
              title: Center(
                child: const Text(
                  'Contact',
                  style: TextStyle(color: Colors.black),
                ),
              ),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Center(
                    child: Text(
                      'Author:',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      ),
                    ),
                  ),
                  Center(
                    child: Text(
                      'Piotr Cała',
                      style: TextStyle(
                        color: Colors.black,
                      ),
                    ),
                  ),
                  Center(
                    child: Text(
                      'Mobile:',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      ),
                    ),
                  ),
                  Center(
                    child: Theme(
                      data: ThemeData(
                        textSelectionTheme: TextSelectionThemeData(
                          selectionColor: Colors.grey,
                        ),
                      ),
                      child: SelectableText(
                        '+48 661 242 256',
                        style: TextStyle(color: Colors.black),
                      ),
                    ),
                  ),
                  Center(
                    child: Text(
                      'E-Mail:',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      ),
                    ),
                  ),
                  Center(
                    child: Theme(
                      data: ThemeData(
                        textSelectionTheme: TextSelectionThemeData(
                          selectionColor: Colors.grey,
                        ),
                      ),
                      child: SelectableText(
                        'piotr.cala@pioxis.com',
                        style: TextStyle(
                          color: Colors.black,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              actions: <Widget>[
                TextButton(
                  onPressed: () => Navigator.pop(context, 'OK'),
                  child: const Text(
                    'OK',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                )
              ],
            ),
          ),
          child: Column(
            children: <Widget>[
              Icon(icon),
              Text(label),
            ],
          ),
          style: ElevatedButton.styleFrom(
              minimumSize: minimumSizeValue, elevation: 30),
        ),
      ),
    ],
  );
}

Column _exitButtonAction(Color color, IconData icon, String label,
    Size minimumSizeValue, BuildContext butCont) {
  return Column(
    mainAxisSize: MainAxisSize.min,
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Container(
        margin: const EdgeInsets.only(top: 12),
        child: ElevatedButton(
          onPressed: () {
            print('Pressed: ' + label);
            Future.delayed(const Duration(milliseconds: 1000), () {
              exit(0);
            });
          },
          child: Column(
            children: <Widget>[
              Icon(icon),
              Text(label),
            ],
          ),
          style: ElevatedButton.styleFrom(
              minimumSize: minimumSizeValue, elevation: 30),
        ),
      ),
    ],
  );
}

//Set icon path string depends of system theme
String iconString(BuildContext contB) {
  var brightness = MediaQuery.of(contB).platformBrightness;
  bool isDarkMode = brightness == Brightness.dark;
  if (isDarkMode)
    return 'images/flutter_cash/iconB.png';
  else
    return 'images/flutter_cash/iconBL.png';
}
