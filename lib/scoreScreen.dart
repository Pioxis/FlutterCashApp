import 'package:flutter/material.dart';
import 'package:pioxis_app/points.dart';

Points scoreBoardMain = Points(-1, -1, -1);
bool visibleS = false;
bool firstLoad = false;

class StepperPresent extends StatefulWidget {
  @override
  _StepperPresent createState() => _StepperPresent();
}

class _StepperPresent extends State<StepperPresent> {
  int _currentStep = 0;
  StepperType stepperType = StepperType.vertical;

  @override
  Widget build(BuildContext contStepper) {
    //final reloadFromSet = ModalRoute.of(context)?.settings.arguments as bool;
    if (!firstLoad) {
      scoreBoardMain.splitToList(Points.readScoreString());
      firstLoad = true;
    }
    return Scaffold(
      appBar: AppBar(
        //automaticallyImplyLeading: false,
        title: Text('Score Board'),
        centerTitle: true,
        actions: <Widget>[
          ElevatedButton.icon(
            icon: Icon(Icons.home_rounded),
            onPressed: () {
              Navigator.pushNamedAndRemoveUntil(context, '/', (_) => false);
            },
            label: Text(''),
          )
        ],
      ),
      body: Container(
        child: Column(
          children: [
            Visibility(
              visible: !visibleS,
              child: Center(
                child: ElevatedButton.icon(
                  style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.deepOrange),
                  icon: Icon(
                    Icons.downloading_rounded,
                    size: 35,
                  ),
                  label: Text(
                    'Load Score',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                  onPressed: () {
                    setState(() {
                      visibleS = !visibleS;
                    });
                  },
                ),
              ),
            ),
            Visibility(
              visible: visibleS,
              child: Expanded(
                child: Theme(
                  data: ThemeData(
                    cardTheme: Theme.of(context)
                        .cardTheme
                        .copyWith(color: Colors.transparent),
                    colorScheme: Theme.of(context).colorScheme.copyWith(
                          background: Colors.red,
                        ),
                    canvasColor: Theme.of(context).canvasColor,
                  ),
                  child: Stepper(
                    controlsBuilder: (context, details) {
                      return Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        child: Row(
                          children: [
                            Visibility(
                              visible: _currentStep != 0,
                              child: Expanded(
                                child: ElevatedButton(
                                  child: Text(
                                    "Back",
                                    style: TextStyle(fontSize: 18),
                                  ),
                                  onPressed: details.onStepCancel,
                                ),
                              ),
                            ),
                            Visibility(
                              child: Spacer(flex: 1),
                              visible: _currentStep != 0,
                            ),
                            Expanded(
                              child: ElevatedButton(
                                onPressed: details.onStepContinue,
                                child: Text(
                                  "Next",
                                  style: TextStyle(fontSize: 18),
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                    type: stepperType,
                    physics: ScrollPhysics(),
                    currentStep: _currentStep,
                    onStepTapped: (step) => tapped(step),
                    onStepContinue: continued,
                    onStepCancel: cancel,
                    steps: <Step>[
                      Step(
                        title: new Text(
                          '1\u02e2\u1d57',
                          style: TextStyle(color: Colors.grey),
                        ),
                        content: Center(
                          child: Text(
                            '${scoreBoardMain.scoreS[0]}',
                            style: TextStyle(fontSize: 35),
                          ),
                        ),
                        isActive: _currentStep >= 0,
                        state: _currentStep >= 0
                            ? StepState.complete
                            : StepState.disabled,
                      ),
                      Step(
                        title: new Text(
                          '2\u207f\u1d48',
                          style: TextStyle(color: Colors.grey),
                        ),
                        content: Center(
                          child: Text(
                            '${scoreBoardMain.scoreS[1]}',
                            style: TextStyle(fontSize: 35),
                          ),
                        ),
                        isActive: _currentStep >= 0,
                        state: _currentStep >= 1
                            ? StepState.complete
                            : StepState.disabled,
                      ),
                      Step(
                        title: new Text(
                          '3\u02b3\u1d48',
                          style: TextStyle(color: Colors.grey),
                        ),
                        content: Center(
                          child: Text(
                            '${scoreBoardMain.scoreS[2]}',
                            style: TextStyle(fontSize: 35),
                          ),
                        ),
                        isActive: _currentStep >= 1,
                        state: _currentStep >= 2
                            ? StepState.complete
                            : StepState.disabled,
                      ),
                      Step(
                        title: new Text(
                          '4\u1d57\u02b0',
                          style: TextStyle(color: Colors.grey),
                        ),
                        content: Center(
                          child: Text(
                            '${scoreBoardMain.scoreS[3]}',
                            style: TextStyle(fontSize: 35),
                          ),
                        ),
                        isActive: _currentStep >= 2,
                        state: _currentStep >= 3
                            ? StepState.complete
                            : StepState.disabled,
                      ),
                      Step(
                        title: new Text(
                          '5\u1d57\u02b0',
                          style: TextStyle(color: Colors.grey),
                        ),
                        content: Center(
                          child: Text(
                            '${scoreBoardMain.scoreS[4]}',
                            style: TextStyle(fontSize: 35),
                          ),
                        ),
                        isActive: _currentStep >= 3,
                        state: _currentStep >= 4
                            ? StepState.complete
                            : StepState.disabled,
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.list),
        onPressed: switchStepsType,
      ),
    );
  }

  switchStepsType() {
    setState(
      () => stepperType == StepperType.vertical
          ? stepperType = StepperType.horizontal
          : stepperType = StepperType.vertical,
    );
  }

  tapped(int step) {
    setState(() => _currentStep = step);
  }

  continued() {
    _currentStep < 4 ? setState(() => _currentStep += 1) : null;
  }

  cancel() {
    _currentStep > 0 ? setState(() => _currentStep -= 1) : null;
  }
}
