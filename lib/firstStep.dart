import 'dart:math';
import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pioxis_app/points.dart'; // pi()

class CashValue {
  int b200, b100, b50, b20, b10, c5, c2, c1;

  CashValue(this.b200, this.b100, this.b50, this.b20, this.b10, this.c5,
      this.c2, this.c1);
}

CashValue amountCashRandom(CashValue val) {
  Random random = new Random();
  //---50---
  val.b50 = random.nextInt(2);
  //---10---
  val.b10 = random.nextInt(4) + 1;
  //---20---
  if (val.b10 >= 2) {
    val.b20 = 0;
  } else {
    val.b20 = random.nextInt(3) + 1;
  }
  //---5---
  val.c5 = random.nextInt(5) + 1;
  //---2---
  val.c2 = random.nextInt(4) + 1;
  //---1---
  val.c1 = random.nextInt(5) + 2;
  //---100---
  if (val.b50 == 0) {
    val.b100 = random.nextInt(2);
  } else {
    val.b100 = 0;
  }
  //---200---
  if (val.b100 == 0 && val.b10 <= 3 && val.b50 == 0) {
    val.b200 = random.nextInt(2);
  } else {
    val.b200 = 0;
  }
  return val;
} //1 alg

int walletValue(CashValue wVal) {
  return ((wVal.b200 * 200) +
      (wVal.b100 * 100) +
      (wVal.b50 * 50) +
      (wVal.b20 * 20) +
      (wVal.b10 * 10) +
      (wVal.c5 * 5) +
      (wVal.c2 * 2) +
      (wVal.c1));
}

int walletCoinsAmount(CashValue val) {
  return val.b200 +
      val.b100 +
      val.b50 +
      val.b20 +
      val.b10 +
      val.c5 +
      val.c2 +
      val.c1;
}

bool firstRoll = false;

Points gamePoints = Points(0, 0, 0);
var cashAmount = CashValue(0, 0, 0, 0, 0, 0, 0, 0);
var cashWalletAmount = CashValue(0, 0, 0, 0, 0, 0, 0, 0);
//controller to control on value from TextField
TextEditingController valueController = new TextEditingController();

class FirstStep extends StatelessWidget {
  FirstStep({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
      //context allowing to disable pop back
      onWillPop: () async => false,
      child: new Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          elevation: 20,
          centerTitle: true,
          title: Text('Wallet'),
          actions: <Widget>[
            Tooltip(
              //Help button
              enableFeedback: true,
              message: 'Tap for HELP',
              child: ElevatedButton(
                onPressed: () => showDialog<String>(
                  context: context,
                  builder: (BuildContext context) => AlertDialog(
                    title: Center(
                      child: const Text(
                        'Help',
                        style: TextStyle(color: Colors.black),
                      ),
                    ),
                    content: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Card(
                          elevation: 10,
                          child: ListTile(
                            title: Center(
                              child: Row(
                                children: <Widget>[
                                  Transform.rotate(
                                    angle: 90 * math.pi / 180,
                                    alignment: Alignment.center,
                                    child: Icon(
                                      Icons.touch_app_rounded,
                                      size: 35,
                                    ),
                                  ),
                                  Tooltip(
                                    enableFeedback: true,
                                    message:
                                        'Tap to collect money to your wallet',
                                    child: ElevatedButton(
                                      onPressed: () => false,
                                      child: SvgPicture.asset(
                                        'images/flutter_cash/bg/200ogc_t.svg',
                                        height: 35,
                                      ),
                                      style: ElevatedButton.styleFrom(
                                        elevation: 35,
                                        backgroundColor: Colors.transparent,
                                      ),
                                    ),
                                  )
                                ],
                                mainAxisAlignment: MainAxisAlignment.center,
                              ),
                            ),
                            leading: Text(
                              '1',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 25),
                            ),
                            visualDensity: VisualDensity.compact,
                            horizontalTitleGap: 1,
                            trailing: SizedBox.shrink(),
                          ),
                        ),
                        Card(
                          elevation: 10,
                          child: ListTile(
                            dense: true,
                            title: Center(
                              child: Row(
                                children: <Widget>[
                                  SvgPicture.asset(
                                    'images/flutter_cash/bg/20ogc_t.svg',
                                    height: 30,
                                  ),
                                  Icon(Icons.add),
                                  SvgPicture.asset(
                                    'images/flutter_cash/coins/5ocgS.svg',
                                    height: 30,
                                  ),
                                  Text(
                                    ' = ',
                                    style: TextStyle(fontSize: 20),
                                  ),
                                  Text(
                                    '25',
                                    style: TextStyle(fontSize: 24),
                                  )
                                ],
                                mainAxisAlignment: MainAxisAlignment.center,
                              ),
                            ),
                            leading: Text(
                              '2',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 25),
                            ),
                            visualDensity: VisualDensity.compact,
                            horizontalTitleGap: 1,
                          ),
                        ),
                        Card(
                          elevation: 10,
                          child: ListTile(
                            title: Center(
                              child: Row(
                                children: <Widget>[
                                  Transform.rotate(
                                    angle: 90 * math.pi / 180,
                                    alignment: Alignment.center,
                                    child: Icon(
                                      Icons.touch_app_rounded,
                                      size: 35,
                                    ),
                                  ),
                                  Tooltip(
                                    enableFeedback: true,
                                    message:
                                        'Type collected money value to verify',
                                    child: ElevatedButton.icon(
                                      onPressed: () => false,
                                      icon: Icon(
                                        Icons.account_balance_wallet_rounded,
                                      ),
                                      label: Text('Wallet'),
                                      style: ElevatedButton.styleFrom(
                                        elevation: 35,
                                        padding: const EdgeInsets.symmetric(
                                          horizontal: 5,
                                          vertical: 2,
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                                mainAxisAlignment: MainAxisAlignment.center,
                              ),
                            ),
                            leading: Text(
                              '3',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 25),
                            ),
                            visualDensity: VisualDensity.compact,
                            horizontalTitleGap: 1,
                            trailing: SizedBox.shrink(),
                          ),
                        ),
                        Card(
                          elevation: 10,
                          child: ListTile(
                            title: Center(
                              child: Row(
                                children: <Widget>[
                                  Transform(
                                    transform: Matrix4.rotationY(
                                      math.pi,
                                    ),
                                    //mirror
                                    alignment: Alignment.center,
                                    //icon with gradient color
                                    child: Icon(
                                      Icons.border_color_rounded,
                                      size: 25,
                                    ),
                                  ),
                                  Icon(Icons.arrow_right_rounded),
                                  RadiantGradientMask(
                                    child: Icon(
                                      Icons.model_training_rounded,
                                      size: 27,
                                    ),
                                  ),
                                  Icon(
                                    Icons.arrow_right_rounded,
                                  ),
                                  Icon(
                                    Icons.flaky_rounded,
                                    color: Colors.green,
                                  ),
                                  Icon(
                                    Icons.flaky,
                                    color: Colors.transparent,
                                  )
                                ],
                                mainAxisAlignment: MainAxisAlignment.center,
                              ),
                            ),
                            leading: Text(
                              '4',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 25),
                            ),
                            visualDensity: VisualDensity.compact,
                            horizontalTitleGap: 1,
                            //trailing: null,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                style: ElevatedButton.styleFrom(
                    elevation: 10, shadowColor: Colors.grey),
                child: Text(
                  '?',
                  style: TextStyle(fontSize: 35),
                ),
              ),
            )
          ],
        ),
        body: mainScreen(context),
        //Reset money and wallet amount
        floatingActionButtonLocation: disabledButtonA(cashAmount)
            ? FloatingActionButtonLocation.centerFloat
            : FloatingActionButtonLocation.miniCenterFloat,
        floatingActionButton: disabledButtonA(cashAmount)
            ? FloatingActionButton.large(
                tooltip: 'Tap to generate money amount',
                enableFeedback: true,
                elevation: 15,
                backgroundColor: Colors.green,
                splashColor: Colors.lightGreenAccent,
                onPressed: () {
                  firstRoll = true;
                  cashAmount = amountCashRandom(cashAmount);
                  cashWalletAmount = CashValue(0, 0, 0, 0, 0, 0, 0, 0);
                  Navigator.pushNamedAndRemoveUntil(
                    context,
                    'startback',
                    (_) => false,
                  );
                  print('Reset wallet value');
                },
                child: Icon(
                  Icons.payments_rounded,
                  size: 52,
                ),
              )
            : FloatingActionButton(
                tooltip: 'Tap to reset money amount',
                enableFeedback: true,
                elevation: 15,
                backgroundColor: Colors.green,
                splashColor: Colors.lightGreenAccent,
                onPressed: () {
                  firstRoll = true;
                  cashAmount = amountCashRandom(cashAmount);
                  cashWalletAmount = CashValue(0, 0, 0, 0, 0, 0, 0, 0);
                  Navigator.pushNamedAndRemoveUntil(
                    context,
                    'startback',
                    (_) => false,
                  );
                },
                child: Icon(Icons.payments_rounded),
              ),
        drawer: Drawer(
          child: ListView(
            children: [
              const SizedBox(
                height: 60.0,
                child: DrawerHeader(
                  child: Text(
                    'Menu',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 19),
                  ),
                ),
              ),
              Card(
                elevation: 5,
                child: ListTile(
                  leading: Icon(Icons.save),
                  title: Text('Settings'),
                  onTap: () {
                    Navigator.pushNamed(
                      context,
                      '/settings',
                    );
                  },
                ),
              ),
              Card(
                elevation: 5,
                child: ListTile(
                  leading: Icon(Icons.cancel),
                  title: Text('Exit to Menu'),
                  onTap: () {
                    Navigator.pushNamedAndRemoveUntil(
                      context,
                      '/',
                      (_) => false,
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class MyDialog extends StatefulWidget {
  const MyDialog({Key? key}) : super(key: key);

  @override
  _MyDialogState createState() => new _MyDialogState();
}

class _MyDialogState extends State<MyDialog> {
  bool _isPressed = false;
  var colorS = borderColor(cashWalletAmount);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Center(
        child: const Text(
          'Wallet Value',
          style: TextStyle(color: Colors.black),
        ),
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Center(
            child: TextField(
              showCursor: true,
              controller: valueController,
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.allow(RegExp('[0-9]')),
                //allow type only digits from 0 to 9
              ],
              style: TextStyle(
                color: Colors.black87,
                fontWeight: FontWeight.bold,
              ),
              decoration: InputDecoration(
                floatingLabelStyle: TextStyle().copyWith(color: Colors.black),
                labelText: 'Value',
                labelStyle: TextStyle().copyWith(color: Colors.black54),
                alignLabelWithHint: true,
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8.0),
                  borderSide: BorderSide(width: 2, color: colorS),
                ),
                hintText: 'Type value here',
                hintStyle: TextStyle().copyWith(
                  fontWeight: FontWeight.normal,
                  color: Colors.black54,
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8.0),
                  borderSide: BorderSide(
                    width: 1,
                    color: Colors.black,
                  ),
                ),
                prefixIcon: const Icon(
                  Icons.account_balance_wallet,
                  color: Colors.black54,
                ),
                suffixStyle: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.black54,
                ),
                suffixText: '&',
              ),
            ),
          ),
        ],
      ),
      actions: <Widget>[
        Tooltip(
          message:
              'Green - correct wallet value\nRed - incorrect wallet value\nBlue - empty value',
          enableFeedback: true,
          child: ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              elevation: 15,
              backgroundColor: colorS,
            ),
            onPressed: _isPressed == false
                ? () async => {
                      setState(() {
                        colorS = borderColor(cashWalletAmount);
                      }),
                      if (colorS == Colors.green)
                        {
                          _isPressed = true,
                          await Future<void>.delayed(
                            const Duration(milliseconds: 700),
                            () {
                              Navigator.pop(context);
                            },
                          )
                        }
                    }
                : null,
            //to prevent multiple run future func => causes trying to run null value
            label: const Text('Check'),
            icon: Icon(Icons.check),
          ),
        )
      ],
    );
  }
}

@override
Widget mainScreen(BuildContext contM) {
  var walletCashAm = walletValue(cashWalletAmount);
  Color cashColor;
  BlendMode cashBlend;
  var finalPointsValue = 0;
  if (firstRoll == false) {
    //grayscale for svgassets
    cashColor = Colors.white10;
    cashBlend = BlendMode.modulate;
  } else {
    //turn off grayscale for svgassets
    cashColor = Colors.transparent;
    cashBlend = BlendMode.saturation;
  }
  return Center(
    child: Column(
      children: <Widget>[
        Expanded(
          flex: 10,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              moneyButton(contM, cashButtonDisable(cashAmount, 200),
                  'bg/200ogc_t.svg', 100, '200 &', cashAmount.b200, 200),
              moneyButton(contM, cashButtonDisable(cashAmount, 100),
                  'bg/100ogc_t.svg', 100, '100 &', cashAmount.b100, 100),
              moneyButton(contM, cashButtonDisable(cashAmount, 50),
                  'bg/50ogc_t.svg', 100, '50 &', cashAmount.b50, 50),
              moneyButton(contM, cashButtonDisable(cashAmount, 20),
                  'bg/20ogc_t.svg', 100, '20 &', cashAmount.b20, 20),
              moneyButton(contM, cashButtonDisable(cashAmount, 10),
                  'bg/10ogc_t.svg', 100, '10 &', cashAmount.b10, 10),
            ],
          ),
        ),
        Expanded(
          flex: 10,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              moneyButton(
                contM,
                cashButtonDisable(cashAmount, 5),
                'coins/5ocgS.svg',
                75,
                '5 &',
                cashAmount.c5,
                5,
              ),
              moneyButton(
                contM,
                cashButtonDisable(cashAmount, 2),
                'coins/2ocg.svg',
                75,
                '2 &',
                cashAmount.c2,
                2,
              ),
              moneyButton(
                contM,
                cashButtonDisable(cashAmount, 1),
                'coins/1ocg.svg',
                75,
                '1 &',
                cashAmount.c1,
                1,
              )
            ],
          ),
        ),
        Visibility(
          visible: false,
          child: Expanded(
            flex: 1,
            child: Column(
              children: <Widget>[
                Center(
                  child: Text(
                    '~DEBUG~ Table: ' +
                        (walletValue(cashAmount)).toString() +
                        ' Wallet: ' +
                        (walletCashAm).toString() +
                        ' Total: ' +
                        (walletValue(cashAmount) + walletCashAm).toString(),
                    style: TextStyle(
                      color: Colors.yellow,
                      backgroundColor: Colors.blueAccent,
                      fontSize: 12.3,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        Visibility(
          visible: firstRoll,
          child: Expanded(
            flex: 3,
            child: Column(
              children: <Widget>[
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.toll_rounded,
                        size: 35,
                        color: Colors.blueGrey,
                      ),
                      Text(' '),
                      Icon(
                        Icons.money_rounded,
                        size: 35,
                        color: Colors.green.shade700,
                      )
                    ],
                  ),
                ),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.save_alt_rounded,
                        size: 75,
                        color: Colors.deepOrangeAccent,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        Expanded(
          flex: 16,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Visibility(
                  visible: disabledButtonA(cashAmount) ? false : true,
                  child: Stack(
                    alignment: Alignment.bottomCenter,
                    children: <Widget>[
                      Center(
                        child: SvgPicture.asset(
                          'images/flutter_cash/wallet.svg',
                          height: 125,
                          colorBlendMode: cashBlend,
                          color: cashColor,
                        ),
                      ),
                      Text(
                        walletCoinsAmount(cashWalletAmount).toString(),
                        //how many coins in wallet
                        style: firstRoll
                            ? TextStyle(
                                fontWeight: FontWeight.normal,
                                color: Colors.orangeAccent,
                                fontSize: 45,
                              )
                            : TextStyle(
                                fontWeight: FontWeight.normal,
                                color: Colors.transparent,
                                fontSize: 45,
                              ),
                      ),
                    ],
                  ),
                ),
                Text(''), //space between card and lower panel
                Visibility(
                  visible: !firstRoll,
                  child: Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Get money and count it',
                          style: TextStyle(
                            fontSize: 25,
                            color: Colors.deepOrange,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Icon(
                          Icons.south_rounded,
                          size: 75,
                          color: Colors.deepOrange,
                        ),
                      ],
                    ),
                  ),
                ),
                Card(
                  //elevation: 5,
                  child: ListTile(
                    title: Container(
                      child: Center(
                        child: Visibility(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Tooltip(
                                message: 'Type here value of collected money',
                                enableFeedback: true,
                                child: ElevatedButton.icon(
                                  onPressed: disabledButtonA(cashAmount)
                                      ? null
                                      : () => showDialog(
                                            context: contM,
                                            builder: (BuildContext context) {
                                              print(walletCashAm.toString());
                                              return MyDialog();
                                            },
                                          ),
                                  label: Text('Wallet'),
                                  icon: Icon(Icons.account_balance_wallet),
                                  style: ElevatedButton.styleFrom(
                                    elevation: 15,
                                    padding: EdgeInsets.symmetric(
                                      horizontal: 30,
                                    ),
                                  ),
                                ),
                              ),
                              Spacer(flex: 1),
                              Directionality(
                                textDirection: TextDirection.rtl,
                                child: Tooltip(
                                  message: 'Go to shop and buy products',
                                  enableFeedback: true,
                                  child: ElevatedButton.icon(
                                    onPressed: disabledButton(
                                            borderColor(cashWalletAmount))
                                        ? null
                                        : () => [
                                              finalPointsValue = (2500 /
                                                      (walletCoinsAmount(
                                                              cashAmount) +
                                                          walletCoinsAmount(
                                                              cashWalletAmount)))
                                                  .ceil(),
                                              gamePoints.firstStep =
                                                  (walletCoinsAmount(
                                                          cashWalletAmount) *
                                                      finalPointsValue),
                                              debugPrint(
                                                'I added to points: ${gamePoints.firstStep}',
                                              ),
                                              Navigator.pushNamedAndRemoveUntil(
                                                contM,
                                                '/second',
                                                (_) => false,
                                                arguments: {
                                                  'cashWalletAmount':
                                                      cashWalletAmount,
                                                  'gamePoints': gamePoints,
                                                },
                                              ),
                                            ],
                                    label: Text('Next'),
                                    icon: Icon(Icons.shopping_cart),
                                    style: ElevatedButton.styleFrom(
                                      elevation: 15,
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 30,
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                          visible: disabledButtonA(cashAmount) ? false : true,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    ),
  );
}

Widget moneyButton(BuildContext contMain, bool visibleVar, String moneyLoc,
    double iconSizeVar, String labelName, int amountOfCoins, int mVal) {
  return Visibility(
    visible: !visibleVar,
    child: Expanded(
      child: Column(
        children: <Widget>[
          IconButton(
            splashRadius: 40,
            tooltip: labelName,
            enableFeedback: true,
            onPressed: () {
              //decrement here and move to wallet
              cashButtonDecrement(cashAmount, mVal, cashWalletAmount);
              Navigator.pushNamedAndRemoveUntil(
                contMain,
                'startback',
                (_) => false,
              );
            },
            icon: SvgPicture.asset(
              'images/flutter_cash/$moneyLoc',
              height: iconSizeVar,
            ),
            iconSize: iconSizeVar,
          ),
          Text(
            '$amountOfCoins',
            style: TextStyle(fontWeight: FontWeight.bold),
          )
        ],
      ),
    ),
  );
}

bool disabledButton(Color bColor) {
  if (bColor != Colors.green || walletValue(cashWalletAmount) == 0) {
    return true;
  } else {
    return false;
  }
}

bool disabledButtonA(CashValue cashV) {
  if (walletValue(cashV) == 0 && firstRoll == false) {
    return true;
  } else {
    return false;
  }
}

void cashButtonDecrement(CashValue valueCash, int cVal, CashValue walletCash,
    [CashValue? secCash]) {
  switch (cVal) {
    case 200:
      if (valueCash.b200 > 0) {
        valueCash.b200 -= 1;
        walletCash.b200 += 1;
        secCash?.b200 += 1;
      }
      break;
    case 100:
      if (valueCash.b100 > 0) {
        valueCash.b100 -= 1;
        walletCash.b100 += 1;
        secCash?.b100 += 1;
      }
      break;
    case 50:
      if (valueCash.b50 > 0) {
        valueCash.b50 -= 1;
        walletCash.b50 += 1;
        secCash?.b50 += 1;
      }
      break;
    case 20:
      if (valueCash.b20 > 0) {
        valueCash.b20 -= 1;
        walletCash.b20 += 1;
        secCash?.b20 += 1;
      }
      break;
    case 10:
      if (valueCash.b10 > 0) {
        valueCash.b10 -= 1;
        walletCash.b10 += 1;
        secCash?.b10 += 1;
      }
      break;
    case 5:
      if (valueCash.c5 > 0) {
        valueCash.c5 -= 1;
        walletCash.c5 += 1;
        secCash?.c5 += 1;
      }
      break;
    case 2:
      if (valueCash.c2 > 0) {
        valueCash.c2 -= 1;
        walletCash.c2 += 1;
        secCash?.c2 += 1;
      }
      break;
    case 1:
      if (valueCash.c1 > 0) {
        valueCash.c1 -= 1;
        walletCash.c1 += 1;
        secCash?.c1 += 1;
      }
      break;
  }
}

bool cashButtonDisable(CashValue valueCash, int cVal) {
  bool temp = false;
  switch (cVal) {
    case 200:
      if (valueCash.b200 == 0) temp = true;
      break;
    case 100:
      if (valueCash.b100 == 0) temp = true;
      break;
    case 50:
      if (valueCash.b50 == 0) temp = true;
      break;
    case 20:
      if (valueCash.b20 == 0) temp = true;
      break;
    case 10:
      if (valueCash.b10 == 0) temp = true;
      break;
    case 5:
      if (valueCash.c5 == 0) temp = true;
      break;
    case 2:
      if (valueCash.c2 == 0) temp = true;
      break;
    case 1:
      if (valueCash.c1 == 0) temp = true;
      break;
    default:
      temp = false;
      break;
  }
  return temp;
}

Color borderColor(CashValue sVal) {
  if (walletValue(sVal).toString() == valueController.text.toString())
    return Colors.green;
  else if (walletValue(sVal).toString() != valueController.text.toString() &&
      valueController.text.toString() != '')
    return Colors.red;
  else
    return Colors.lightBlue;
}

class RadiantGradientMask extends StatelessWidget {
  //Gradient to icon
  RadiantGradientMask({required this.child});

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      shaderCallback: (Rect bounds) {
        return RadialGradient(
          center: Alignment.bottomRight,
          radius: 0.48,
          colors: <Color>[Colors.deepOrange, Colors.green],
          tileMode: TileMode.mirror,
        ).createShader(bounds);
      },
      child: child,
    );
  }
}
