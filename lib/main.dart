import 'package:flutter/material.dart';
import 'package:pioxis_app/firstStep.dart';
import 'package:pioxis_app/scoreScreen.dart';
import 'package:pioxis_app/secondStep.dart';

import 'finalStep.dart';
import 'mainScreenW.dart';
import 'settScreen.dart';

void main() {
  runApp(
    MaterialApp(
      home: MyApp(),
      //debugShowCheckedModeBanner: false,
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      //section to change context without animation pageBuilder: (_, __, ___)
      debugShowCheckedModeBanner: false,
      onGenerateRoute: (settings) {
        if (settings.name == 'startback') {
          return PageRouteBuilder(pageBuilder: (_, __, ___) => FirstStep());
        } else if (settings.name == 'secback') {
          return PageRouteBuilder(pageBuilder: (_, __, ___) => SecondStep());
        } else if (settings.name == 'finalreload') {
          return PageRouteBuilder(pageBuilder: (_, __, ___) => FinalStep());
        } else if (settings.name == 'scoreroll') {
          return PageRouteBuilder(
              pageBuilder: (_, __, ___) => StepperPresent());
        }
        return null;
      },
      initialRoute: '/',
      routes: {
        '/': (context) => FirstScreen(),
        '/settings': (context) => SettingsScreen(),
        'start': (context) => FirstStep(),
        '/second': (context) => SecondStep(),
        '/final': (context) => FinalStep(),
        '/score': (context) => StepperPresent()
      },
      title: 'Cash Trainer',
      darkTheme: ThemeData.dark().copyWith(
        useMaterial3: true,
        colorScheme: ColorScheme.dark(
          primary: Colors.amber,
          //onSurface: Colors.white,
          onPrimary: Colors.black,
          secondary: Colors.amber
        ),
        //dialogBackgroundColor: Colors.amber,
      ),
      theme: ThemeData.light(
        useMaterial3: true,
      ),
      themeMode: ThemeMode.system,
    );
  }
}
