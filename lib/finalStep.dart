import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pioxis_app/firstStep.dart';
import 'package:pioxis_app/points.dart';
import 'package:pioxis_app/secondStep.dart';
import 'package:restart_app/restart_app.dart';

var recieveWalletCash = new CashValue(0, 0, 0, 0, 0, 0, 0, 0);
var cashToCashmachine = new CashValue(0, 0, 0, 0, 0, 0, 0, 0);
var changeCoins = new CashValue(0, 0, 0, 0, 0, 0, 0, 0);
var changeInWallet = new CashValue(0, 0, 0, 0, 0, 0, 0, 0);
var changeCoinsVerify = new CashValue(0, 0, 0, 0, 0, 0, 0, 0);
int changeTableAmount = 0;
Points gamePointsFinal = Points(0, 0, 0);
List<ProductList> recieveCartList = new List<ProductList>.empty();
bool exitEnter = false;
bool endP = false;
bool noMoneyRoll = false;
bool endGame = false;
TextEditingController valueControllerF = new TextEditingController();

class MyDialogS extends StatefulWidget {
  const MyDialogS({Key? key}) : super(key: key);

  @override
  _MyDialogStateS createState() => new _MyDialogStateS();
}

class _MyDialogStateS extends State<MyDialogS> {
  bool _isPressed = false;
  var colorSF = borderColorF(changeCoinsVerify);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Center(
        child: const Text(
          'Change Value',
          style: TextStyle(color: Colors.black),
        ),
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Center(
            child: TextField(
              showCursor: true,
              controller: valueControllerF,
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.allow(RegExp('[0-9]'))
                //allow type only digits from 0 to 9
              ],
              style: TextStyle(
                color: Colors.black87,
                fontWeight: FontWeight.bold,
              ),
              decoration: InputDecoration(
                floatingLabelStyle: TextStyle().copyWith(color: Colors.black),
                labelText: 'Value',
                labelStyle: TextStyle().copyWith(color: Colors.black54),
                alignLabelWithHint: true,
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8.0),
                  borderSide: BorderSide(width: 2, color: colorSF),
                ),
                hintText: 'Type value here',
                hintStyle: TextStyle().copyWith(
                    fontWeight: FontWeight.normal, color: Colors.black54),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8.0),
                  borderSide: BorderSide(width: 1, color: Colors.black),
                ),
                prefixIcon: const Icon(
                  Icons.account_balance_wallet,
                  color: Colors.black54,
                ),
                suffixStyle: TextStyle(
                    fontWeight: FontWeight.bold, color: Colors.black54),
                suffixText: '&',
              ),
            ),
          ),
        ],
      ),
      actions: <Widget>[
        Tooltip(
          message: 'Green - correct wallet value\nRed - incorrect wallet value'
              '\nBlue - empty value',
          enableFeedback: true,
          child: ElevatedButton.icon(
            style: ElevatedButton.styleFrom(
              elevation: 15,
              backgroundColor: colorSF,
            ),
            onPressed: _isPressed == false
                ? () async => {
                      setState(() {
                        colorSF = borderColorF(changeCoinsVerify);
                      }),
                      if (colorSF == Colors.green)
                        {
                          _isPressed = true,
                          setState(() {
                            endGame = true;
                          }),
                          await Future<void>.delayed(
                            const Duration(milliseconds: 700),
                            () {
                              debugPrint('Wykonanie ENDGAME');
                              gamePointsFinal.finalStep = 500;
                              gamePointsFinal
                                  .splitToList(Points.readScoreString());
                              gamePointsFinal.addToScoreBoard();
                              gamePointsFinal.writeToFile();
                              Navigator.pop(context);
                            },
                          )
                        }
                    }
                : null,
            //to prevent multiple run future func => causes trying to run null value
            label: const Text('Check'),
            icon: Icon(Icons.check),
          ),
        )
      ],
    );
  }
}

Color borderColorF(CashValue sVals) {
  if (walletValue(sVals).toString() == valueControllerF.text.toString()) {
    return Colors.green;
  } else if (walletValue(sVals).toString() !=
          valueControllerF.text.toString() &&
      valueControllerF.text.toString() != '') {
    return Colors.red;
  } else {
    return Colors.lightBlue;
  }
}

class FinalStep extends StatefulWidget {
  @override
  _FinalStep createState() => _FinalStep();
}

class _FinalStep extends State<FinalStep> {
  @override
  Widget build(BuildContext context) {
    if (exitEnter == false) {
      final argumentsRecevied =
          ModalRoute.of(context)?.settings.arguments as Map;
      //insert object to var
      recieveWalletCash = argumentsRecevied['walletCashVar'];
      //insert recevied list to list obj
      recieveCartList = argumentsRecevied['shoppingCartList'];
      gamePointsFinal = argumentsRecevied['_gamePointsSecond'];
      debugPrint('Wartosc portfela: ${walletValue(recieveWalletCash)}');
      exitEnter = true;
      if (walletValue(recieveWalletCash) <
          ProductList.sumVal(recieveCartList)) {
        noMoneyRoll = true;
      } else {
        noMoneyRoll = false;
      }
    }
    if (noMoneyRoll) {
      return new Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.red,
          centerTitle: true,
          title: Text('Cash Register'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Game Over',
                style: TextStyle(fontSize: 45, fontWeight: FontWeight.bold),
              ),
              Text(
                'Not enough money to buy products.',
                style: TextStyle(fontSize: 20),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('Money in wallet: '),
                  Text(
                    '${walletValue(recieveWalletCash)} &',
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                    ),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('Cost of products: '),
                  Text(
                    '${ProductList.sumVal(recieveCartList)} &',
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                    ),
                  )
                ],
              ),
              ElevatedButton.icon(
                onPressed: () {
                  gamePointsFinal.finalStep = 0;
                  gamePointsFinal.addToScoreBoard();
                  Restart.restartApp(webOrigin: '/');
                },
                style: ElevatedButton.styleFrom(backgroundColor: Colors.red),
                label: Text('New Game'),
                icon: Icon(Icons.autorenew_rounded),
              )
            ],
          ),
        ),
      );
    } else {
      bool send;
      return new WillPopScope(
        onWillPop: () async => false,
        child: new Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            title: Text('Cash Register'),
            centerTitle: true,
            elevation: 20,
            actions: <Widget>[
              Tooltip(
                message: 'Value of cash in your wallet',
                enableFeedback: true,
                child: ElevatedButton.icon(
                  onPressed: () {},
                  style: ElevatedButton.styleFrom(elevation: 15),
                  icon: Icon(Icons.account_balance_wallet_rounded),
                  label: Text(
                    endP
                        ? '${walletValue(changeInWallet)}'
                        : '${walletValue(recieveWalletCash)}',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 19,
                    ),
                  ),
                ),
              )
            ],
          ),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
          floatingActionButton: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Visibility(
                  visible: !endP,
                  child: FloatingActionButton.extended(
                    elevation: 15,
                    heroTag: null,
                    icon: Icon(
                      Icons.credit_score_rounded,
                      size: 30,
                    ),
                    label: Text(
                      'End Payment',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    backgroundColor: Colors.green,
                    onPressed: () {
                      endPayment(context);
                      changePoints(); //more points if less coins to get
                    },
                  ),
                ),
                Visibility(
                  visible: endP,
                  child: Directionality(
                    textDirection: TextDirection.rtl,
                    child: FloatingActionButton.extended(
                      elevation: 15,
                      heroTag: null,
                      icon: Icon(
                        Icons.logout_rounded,
                        size: 30,
                      ),
                      label: Text(
                        !endGame ? 'End Game' : 'Game Score',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      backgroundColor: Colors.deepOrange,
                      onPressed: () => [
                        if (!endGame)
                          {
                            showDialog(
                              barrierDismissible: false,
                              context: context,
                              builder: (BuildContext context) {
                                debugPrint('${walletValue(changeCoinsVerify)}');
                                return MyDialogS();
                              },
                            ),
                          },
                        if (endGame)
                          {
                            send = true,
                            Navigator.pushNamedAndRemoveUntil(
                              context,
                              'scoreroll',
                              (_) => false,
                              arguments: send,
                            )
                          }
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
          body: mainScreenFinal(context),
          drawer: Drawer(
            child: ListView(
              //padding: EdgeInsets.zero,
              children: [
                const SizedBox(
                  height: 60.0,
                  child: DrawerHeader(
                    child: Text(
                      'Menu',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 19),
                    ),
                  ),
                ),
                Card(
                  elevation: 5,
                  child: Tooltip(
                    message: 'Restart APP in settings',
                    enableFeedback: true,
                    child: ListTile(
                      leading: Icon(Icons.save),
                      title: Text('Settings'),
                      onTap: () {
                        Navigator.pushNamed(context, '/settings');
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }
  }

  Widget mainScreenFinal(BuildContext contM) {
    return Visibility(
      visible: !endGame,
      child: Center(
        child: Column(
          children: <Widget>[
            //Wallet section
            Expanded(
              flex: 4,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: IconButton(
                      highlightColor: Colors.transparent,
                      splashRadius: 28,
                      tooltip: 'Give money to Shop Assistant',
                      enableFeedback: true,
                      icon: Icon(
                        endP ? Icons.north_rounded : Icons.south_rounded,
                        size: 50,
                        color: Colors.red,
                      ),
                      onPressed: () {},
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Center(
                      child: SvgPicture.asset(
                        'images/flutter_cash/wallet.svg',
                        height: 125,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: IconButton(
                      highlightColor: Colors.transparent,
                      splashRadius: 28,
                      tooltip: 'Give money to Shop Assistant',
                      enableFeedback: true,
                      icon: Icon(
                        endP ? Icons.north_rounded : Icons.south_rounded,
                        size: 50,
                        color: Colors.red,
                      ),
                      onPressed: () {},
                    ),
                  )
                ],
              ),
            ),
            //Money section
            Visibility(
              visible: !endP,
              child: Expanded(
                flex: 6,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        cashButton(
                          contM,
                          noMoney(recieveWalletCash.b200),
                          'bg/200ogc_t.svg',
                          95,
                          '200 &',
                          recieveWalletCash.b200,
                          200,
                          recieveWalletCash,
                          cashToCashmachine,
                        ),
                        cashButton(
                          contM,
                          noMoney(recieveWalletCash.b100),
                          'bg/100ogc_t.svg',
                          95,
                          '100 &',
                          recieveWalletCash.b100,
                          100,
                          recieveWalletCash,
                          cashToCashmachine,
                        ),
                        cashButton(
                          contM,
                          noMoney(recieveWalletCash.b50),
                          'bg/50ogc_t.svg',
                          95,
                          '50 &',
                          recieveWalletCash.b50,
                          50,
                          recieveWalletCash,
                          cashToCashmachine,
                        ),
                        cashButton(
                          contM,
                          noMoney(recieveWalletCash.b20),
                          'bg/20ogc_t.svg',
                          95,
                          '20 &',
                          recieveWalletCash.b20,
                          20,
                          recieveWalletCash,
                          cashToCashmachine,
                        ),
                        cashButton(
                          contM,
                          noMoney(recieveWalletCash.b10),
                          'bg/10ogc_t.svg',
                          95,
                          '10 &',
                          recieveWalletCash.b10,
                          10,
                          recieveWalletCash,
                          cashToCashmachine,
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        cashButton(
                          contM,
                          noMoney(recieveWalletCash.c5),
                          'coins/5ocgS.svg',
                          70,
                          '5 &',
                          recieveWalletCash.c5,
                          5,
                          recieveWalletCash,
                          cashToCashmachine,
                        ),
                        cashButton(
                          contM,
                          noMoney(recieveWalletCash.c2),
                          'coins/2ocg.svg',
                          70,
                          '2 &',
                          recieveWalletCash.c2,
                          2,
                          recieveWalletCash,
                          cashToCashmachine,
                        ),
                        cashButton(
                          contM,
                          noMoney(recieveWalletCash.c1),
                          'coins/1ocg.svg',
                          70,
                          '1 &',
                          recieveWalletCash.c1,
                          1,
                          recieveWalletCash,
                          cashToCashmachine,
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ), //Cash to CashMachine
            Visibility(
              visible: endP,
              child: Expanded(
                flex: 6,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        cashButton(
                          contM,
                          noMoney(changeCoins.b200),
                          'bg/200ogc_t.svg',
                          95,
                          '200 &',
                          changeCoins.b200,
                          200,
                          changeCoins,
                          changeInWallet,
                          changeCoinsVerify,
                        ),
                        cashButton(
                          contM,
                          noMoney(changeCoins.b100),
                          'bg/100ogc_t.svg',
                          95,
                          '100 &',
                          changeCoins.b100,
                          100,
                          changeCoins,
                          changeInWallet,
                          changeCoinsVerify,
                        ),
                        cashButton(
                          contM,
                          noMoney(changeCoins.b50),
                          'bg/50ogc_t.svg',
                          95,
                          '50 &',
                          changeCoins.b50,
                          50,
                          changeCoins,
                          changeInWallet,
                          changeCoinsVerify,
                        ),
                        cashButton(
                          contM,
                          noMoney(changeCoins.b20),
                          'bg/20ogc_t.svg',
                          95,
                          '20 &',
                          changeCoins.b20,
                          20,
                          changeCoins,
                          changeInWallet,
                          changeCoinsVerify,
                        ),
                        cashButton(
                          contM,
                          noMoney(changeCoins.b10),
                          'bg/10ogc_t.svg',
                          95,
                          '10 &',
                          changeCoins.b10,
                          10,
                          changeCoins,
                          changeInWallet,
                          changeCoinsVerify,
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        cashButton(
                          contM,
                          noMoney(changeCoins.c5),
                          'coins/5ocgS.svg',
                          70,
                          '5 &',
                          changeCoins.c5,
                          5,
                          changeCoins,
                          changeInWallet,
                          changeCoinsVerify,
                        ),
                        cashButton(
                          contM,
                          noMoney(changeCoins.c2),
                          'coins/2ocg.svg',
                          70,
                          '2 &',
                          changeCoins.c2,
                          2,
                          changeCoins,
                          changeInWallet,
                          changeCoinsVerify,
                        ),
                        cashButton(
                          contM,
                          noMoney(changeCoins.c1),
                          'coins/1ocg.svg',
                          70,
                          '1 &',
                          changeCoins.c1,
                          1,
                          changeCoins,
                          changeInWallet,
                          changeCoinsVerify,
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ), //Change to Wallet
            //Cashmashine section
            Expanded(
              flex: 5,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Visibility(
                    visible: !endP,
                    child: Text('  '),
                  ),
                  Visibility(
                    visible: !endP,
                    child: Expanded(
                        flex: 2,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(
                                  Icons.record_voice_over_rounded,
                                  size: 35,
                                ),
                                Text(
                                  ' :',
                                  style: TextStyle(fontSize: 25),
                                )
                              ],
                            ),
                            Text(
                              '"You gave me  ',
                              style: TextStyle(
                                fontSize: 16,
                                fontStyle: FontStyle.italic,
                              ),
                            ),
                            Text(
                              '${walletCoinsAmount(cashToCashmachine)}',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 30,
                              ),
                            ),
                            Text(
                              'coins and bills."\n',
                              style: TextStyle(
                                fontSize: 16,
                                fontStyle: FontStyle.italic,
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(Icons.toll_rounded),
                                Text(' + '),
                                Icon(Icons.money_rounded),
                                Icon(Icons.east),
                                Icon(Icons.point_of_sale_rounded)
                              ],
                            ),
                          ],
                        )),
                  ),
                  Expanded(
                    flex: 4,
                    child: Stack(
                      alignment: Alignment.topCenter,
                      children: <Widget>[
                        SvgPicture.asset(
                          'images/flutter_cash/cashmashine.svg',
                          height: 275,
                        ),
                        Text(
                          endP
                              ? '***'
                              : '${ProductList.sumVal(recieveCartList)}',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 73,
                            color: Colors.lightGreenAccent.shade700,
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ), //Message from Shopassistant
            Spacer(
              flex: 1,
            ),
            //Listtile for background section
            Expanded(
                flex: 1,
                child: Card(
                  elevation: 15,
                  child: ListTile(), //background for floatingbutton
                ))
          ],
        ),
      ),
    );
  }

  Widget cashButton(
      BuildContext contMain,
      bool visibleVar,
      String moneyLoc,
      double iconSizeVar,
      String labelName,
      int amountOfCoins,
      int mVal,
      CashValue fromC,
      CashValue toC,
      [CashValue? secC]) {
    return Visibility(
      visible: visibleVar,
      child: Expanded(
        child: Column(
          children: <Widget>[
            IconButton(
              splashRadius: 40,
              tooltip: labelName,
              enableFeedback: true,
              onPressed: () {
                //decrement here
                cashButtonDecrement(fromC, mVal, toC, secC);
                Navigator.pushNamedAndRemoveUntil(
                    contMain, 'finalreload', (_) => false);
              },
              icon: SvgPicture.asset(
                'images/flutter_cash/$moneyLoc',
                height: iconSizeVar,
              ),
              iconSize: iconSizeVar,
            ),
            Text(
              '$amountOfCoins',
              style: TextStyle(fontWeight: FontWeight.bold),
            )
          ],
        ),
      ),
    );
  }

  bool noMoney(int howManyCoins) {
    if (howManyCoins <= 0) {
      return false;
    } else {
      return true;
    }
  }

  void endPayment(BuildContext contPay) {
    if (walletValue(cashToCashmachine) >= ProductList.sumVal(recieveCartList)) {
      generateChange();
      endP = true;
      Navigator.pushNamedAndRemoveUntil(
        contPay,
        'finalreload',
        (_) => false,
      );
    } else {
      endP = false;
      showAlertDialog(
        contPay,
        'Money',
        'You did not give me enough money',
        Colors.red.shade900,
      );
    }
  }

  void generateChange() {
    int tempCash = walletValue(cashToCashmachine) -
        ProductList.sumVal(recieveCartList); //first roll
    while (tempCash > 0) //to end loop while entire change is on table
    {
      if (tempCash >= 200) {
        changeCoins.b200++;
        tempCash = tempCash - 200;
      } else if (tempCash < 200 && tempCash >= 100) {
        changeCoins.b100++;
        tempCash = tempCash - 100;
      } else if (tempCash < 100 && tempCash >= 50) {
        changeCoins.b50++;
        tempCash = tempCash - 50;
      } else if (tempCash < 50 && tempCash >= 20) {
        changeCoins.b20++;
        tempCash = tempCash - 20;
      } else if (tempCash < 20 && tempCash >= 10) {
        changeCoins.b10++;
        tempCash = tempCash - 10;
      } else if (tempCash < 10 && tempCash >= 5) {
        changeCoins.c5++;
        tempCash = tempCash - 5;
      } else if (tempCash < 5 && tempCash >= 2) {
        changeCoins.c2++;
        tempCash = tempCash - 2;
      } else if (tempCash < 2 && tempCash >= 1) {
        changeCoins.c1++;
        tempCash = tempCash - 1;
      }
    }
    changeTableAmount = walletValue(changeCoins);
    //rest money on table from wallet back to wallet
    changeInWallet.b200 += recieveWalletCash.b200;
    changeInWallet.b100 += recieveWalletCash.b100;
    changeInWallet.b50 += recieveWalletCash.b50;
    changeInWallet.b20 += recieveWalletCash.b20;
    changeInWallet.b10 += recieveWalletCash.b10;
    changeInWallet.c5 += recieveWalletCash.c5;
    changeInWallet.c2 += recieveWalletCash.c2;
    changeInWallet.c1 += recieveWalletCash.c1;
  }

  showAlertDialog(
      BuildContext contAlert, String titleA, String contentA, Color bC) {
    showDialog(
      context: contAlert,
      builder: (BuildContext contextA) {
        return AlertDialog(
          backgroundColor: bC,
          title: Center(
            child: Text(
              titleA,
              style: TextStyle(color: Colors.white),
            ),
          ),
          content: FittedBox(
            child: Text(
              contentA,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            ),
          ),
        );
      },
    );
  }

  void changePoints() {
    int changeCoinsAmount = walletCoinsAmount(changeCoins);
    int pointsToReduce = 1000;
    int result = pointsToReduce - (changeCoinsAmount * 100);
    if (result < 0) result = 0;
    gamePointsFinal.finalStep += result;
    debugPrint('Points of change coins on table: ${gamePointsFinal.finalStep}');
  }
}
