# Flutter Cash App

Flutter Cash App is educational application to make child more experienced to buying in real shops.

## Installation

Download .apk file and transfer it to your mobile device. After that install it via your package manager.

## Usage

[![Video Presentation](https://img.youtube.com/vi/ORMGg2WmINc/0.jpg)](https://www.youtube.com/watch?v=ORMGg2WmINc "Flutter Cash App Presentation")

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[GPL3](https://choosealicense.com/licenses/gpl-3.0/)